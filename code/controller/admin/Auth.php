<?php
namespace controller\admin;
use classes\View;
use model\admin as ma;

class Auth {
    public function authentify(){
        $manager = new ma\AuthManager();
        if(empty($_POST['login']) && empty($_POST['password'])){
            $msg = $manager->getMsgCreateAuth();
            $view = new View('admin', 'authentication', 'Connexion', 'connexion');
            $view->displayView(array('msg'=>$msg));
        } else if(empty($_POST['login']) || empty($_POST['password'])){
            $msg = $manager->getMsgCreateAuth();
            $view = new View('admin', 'authentication', 'Connexion', 'connexion');
            $view->displayView(array('msg'=>$msg));
        } else {
            $user = $manager->find($_POST['login'], $_POST['password']);
            if($user === null || $user->getEmailUser() !== $_POST['login'] || $user->getPswdUser() !== $_POST['password']){
                $msg = $manager->getMsgErrorAuth();
                $view = new View('admin', 'authentication', 'Connexion', 'connexion');
                $view->displayView(array('msg'=>$msg));
            }
            else if($user->getEmailUser() === $_POST['login'] && $user->getPswdUser() === $_POST['password']){
                $_SESSION['login'] = $_POST['login'];
                $this->goToDashboard();
            }
        }
    }
    public function goToDashboard(){
        $view = new View('site', 'dashboard', 'tableau de bord', 'tableau de bord');
        $view->displayView(array('login'=>$_SESSION['login']));
    }
}
