<?php
namespace controller\site;
use classes\View;
use model\site as ms;
class CookDelete {
    public function deleteCook(){
        $manager = new ms\CookManager();
        $id = $_GET['id'];
        if(empty($_POST['oui']) && empty($_POST['non'])){
            $cook = $manager->find($id);
        } else if(isset($_POST['non'])){
            header('Location: index.php?p=cooks');
        } else if(isset($_POST['oui'])){
            $cook = $manager->deleteOne($id);
            $msg = $manager->getMsgSuccessDeleteCook();
        }

        $view = new View('site', 'cook-delete', 'deleteCook', 'suppression '.$cook->getFirstnameCook().' '.$cook->getNameCook());
        $view->displayView(array('msg'=>$msg, 'cook'=>$cook));
    }
}
