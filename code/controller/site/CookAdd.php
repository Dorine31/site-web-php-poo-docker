<?php
namespace controller\site;
use classes\View;
use model\site as ms;

class CookAdd {
    public function addCook(){
        $manager = new ms\CookManager();
        if(empty($_POST['lastname']) && empty($_POST['firstname']) && empty($_POST['photoCook'])){
            $msg = $manager->getMsgCreateCook();
        } else if(empty($_POST['lastname']) || empty($_POST['firstname']) || empty($_POST['photoCook'])){
            $msg = $manager->getMsgErrorCook();
        } else {
            $data = $manager->filter();
            $cook = $manager->insert($data);
            $msg = $manager->getMsgSuccessInsertCook();
        }
        $view = new View('site', 'cook-add', 'AddCook', 'ajout d\'un cuisinier');
        $view->displayView(array('cook'=>$cook, 'msg'=>$msg));
    }
}
