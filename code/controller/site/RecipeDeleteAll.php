<?php
namespace controller\site;
use classes\View;
use model\site as ms;

class RecipeDeleteAll {
    public function deleteRecipes(){
        $manager = new ms\RecipeManager();
        $id = $_GET['id'];
        if(isset($_POST['non'])){
            header('Location: index.php?p=home');
        } else if(isset($_POST['oui'])){
            $manager->deleteAllRecipe();
            $msg = $manager->getMsgSuccessDeleteAllRecipe();
        }
        $view = new View('site', 'recipe-delete-all', 'deleteAllRecipe', 'suppression de toutes les recettes');
        $view->displayView(array('msg'=>$msg));
    }
}
