<?php
namespace controller\site;
use classes\View;
use model\site as ms;

class RecipeDelete {
    public function deleteRecipe(){
        $manager = new ms\RecipeManager();
        $id = $_GET['id'];
        if(empty($_POST['oui']) && empty($_POST['non'])){
            $recipe = $manager->readOne($id);
        } else if(isset($_POST['non'])){
            header('Location: index.php?p=home');
        } else if(isset($_POST['oui'])){
            $recipe = $manager->deleteOne($id);
            $msg = $manager->getMsgSuccessDeleteRecipe();
        }
        $view = new View('site', 'recipe-delete', 'deleteRecipe', 'suppression '.$recipe->getTitleRecipe());
        $view->displayView(array('msg'=>$msg, 'recipe'=>$recipe));
    }
}
