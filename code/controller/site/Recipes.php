<?php
namespace controller\site;
use classes\View;

class Recipes {
    public function displayRecipes(){
        $view = new View('site', 'recipes', 'listRecipe', 'liste des recettes');
        $view->displayView();
    }
}
