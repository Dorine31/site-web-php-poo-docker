<?php
namespace controller\site;
use classes\View;
use model\site as ms;
class RecipeAdd {
    public function addRecipe(){
        $manager = new ms\RecipeManager();
        $managerCook = new ms\CookManager();
        $cooks = $managerCook->getAll(100, 1);
        if(empty($_POST['titleRecipe']) && empty($_POST['photoRecipe']) &&
            empty($_POST['difficulty']) && empty($_POST['duration']) &&
            empty($_POST['cost']) && empty($_POST['description']) &&
            empty($_POST['steps']) && empty($_POST['personsNumber']) &&
            empty($_POST['visibility']) && empty($_POST['idCook']) &&
            empty($_POST['ingredients'])) {
            $msg = $manager->getMsgCreateRecipe();
        } else if(empty($_POST['titleRecipe']) || empty($_POST['photoRecipe']) ||
            empty($_POST['difficulty']) || empty($_POST['duration']) ||
            empty($_POST['cost']) || empty($_POST['description']) ||
            empty($_POST['steps']) || empty($_POST['personsNumber']) ||
            empty($_POST['visibility']) || empty($_POST['idCook']) ||
            empty($_POST['ingredients'])){
            $msg = $manager->getMsgErrorRecipe();
        } else {
            $data = $manager->filter();
            $recipe = $manager->insert($data);
            $msg = $manager->getMsgSuccessInsertRecipe();
        }
        $view = new View('site', 'recipe-add', 'addRecipe', 'ajout d\'une recette');
        $view->displayView(array('cooks'=>$cooks, 'recipe'=>$recipe, 'msg'=>$msg));
    }
}
