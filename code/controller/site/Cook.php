<?php
namespace controller\site;
use classes\View;
use model\site\CookManager;

class Cook {
    public function displayCook(){
        $manager = new CookManager();
        $cook = $manager->find($_GET['id']);
        $view = new View('site', 'cook', 'cook', $cook->getFirstnameCook(). ' '.$cook->getNameCook());
        $view->displayView(array('cook'=>$cook));
    }
}
