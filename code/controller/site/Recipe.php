<?php
namespace controller\site;
use classes\View;
use model\site\RecipeManager;

class Recipe {
    public function displayRecipe(){
        $manager = new RecipeManager();
        $recipe = $manager->readOne($_GET['id']);
        $view = new View('site', 'recipe', $recipe->getTitleRecipe(),  $recipe->getTitleRecipe());
        $view->displayView(array('recipe'=>$recipe));
    }
}
