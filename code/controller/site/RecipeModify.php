<?php
namespace controller\site;
use classes\View;
use model\site as ms;

class RecipeModify {
    public function modifyRecipe(){
        $manager = new ms\RecipeManager();
        $id = $_GET['id'];
        $recipe = $manager->readOne($id);
        if(empty($_POST['titleRecipe']) && empty($_POST['photoRecipe']) &&
            empty($_POST['difficulty']) && empty($_POST['duration']) &&
            empty($_POST['cost']) && empty($_POST['description']) &&
            empty($_POST['steps']) && empty($_POST['personsNumber']) &&
            empty($_POST['visibility']) && empty($_POST['idCook']) &&
            empty($_POST['ingredients'])){
            $msg = $manager -> getMsgCreateRecipe();
        } else if(empty($_POST['titleRecipe']) || empty($_POST['photoRecipe']) ||
            empty($_POST['difficulty']) || empty($_POST['duration']) ||
            empty($_POST['cost']) || empty($_POST['description']) ||
            empty($_POST['steps']) || empty($_POST['personsNumber']) ||
            empty($_POST['visibility']) || empty($_POST['idCook']) ||
            empty($_POST['ingredients'])){
            $msg = $manager -> getMsgErrorRecipe();
        } else {
            $data = $manager->filter();
            $recipe = $manager->update($data, $id);
            $msg = $manager->getMsgSuccessModifyRecipe();
        }
        $view = new View('site', 'recipe-modify', 'modifyRecipe', 'modifier '.$recipe->getTitleRecipe());
        $view->displayView(array('msg'=>$msg, 'recipe'=>$recipe));
    }
}
