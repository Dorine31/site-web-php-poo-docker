<?php
namespace controller\site;
use classes\View;;
use model\site as ms;

class Contact {
    public function createContact(){
        $manager = new ms\ContactManager();
        if(empty($_POST['lastname']) && empty($_POST['firstname']) && empty($_POST['email']) && empty($_POST['object'])){
            $msg = $manager->getMsgCreateContact();
        } else {
            $filterArr = $manager->filter();
            $result = $manager->createContact($filterArr);
            $contact = new ms\Contact();
            $contact->setNameContact($result->nameContact);
            $contact->setFirstnameContact($result->firstname);
            $contact->setEmailContact($result->email);
            $contact->setObjectContact($result->object);
            $msg = $manager->getMsgSuccessContact();
        }
        $view = new View('site', 'contact', 'contact', 'contact');
        $view->displayView(array('msg'=>$msg));
    }
}