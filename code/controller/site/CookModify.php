<?php
namespace controller\site;
use classes\View;
use model\site\CookManager;

class CookModify {
    public function modifyCook(){
        $manager = new CookManager();
        $id = $_GET['id'];
        $cook = $manager->find($id);
        if(empty($_POST['lastname']) && empty($_POST['firstname']) && empty($_POST['photoCook'])){
            $msg = $manager -> getMsgCreateCook();
        } else if(empty($_POST['lastname']) || empty($_POST['firstname']) || empty($_POST['photoCook'])){
            $msg = $manager -> getMsgErrorCook();
        } else {
            $data = $manager->filter();
            $cook = $manager->update($data, $id);
            $msg = $manager->getMsgSuccessModifyCook();
        }
        $view = new View('site', 'cook-modify', 'modifier un cuisinier', 'modifier ' .$cook->getFirstnameCook(). ' '.$cook->getNameCook());
        $view->displayView(array('msg'=>$msg, 'cook'=>$cook));
    }
}
