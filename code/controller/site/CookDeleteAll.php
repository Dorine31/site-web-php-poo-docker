<?php
namespace controller\site;
use classes\View;
use model\site as ms;
class CookDeleteAll {
    public function deleteCooks(){
        $manager = new ms\CookManager();
        $id = $_GET['id'];
        if(isset($_POST['non'])){
            header('Location: index.php?p=cooks');
        } else if(isset($_POST['oui'])){
            $manager->deleteAllCook();
            $msg = $manager->getMsgSuccessDeleteAllCooks();
        }
        $view = new View('site', 'cook-delete-all', 'deleteAllCooks', 'suppression de tous les cuisiniers');
        $view->displayView(array('msg'=>$msg));
    }
}
