<?php
namespace controller\site;
use classes\View;
use model\site\CookManager;

class Cooks {
    public function displayCooks(){
        $manager = new CookManager();
        #define and create pages according to the number of recipes to display
        $nbByPage= 10;
        $currentPage = 1;
        $counter = $manager::getTotalNber();
        foreach ($counter as $row){
            $nbCook = $row->total;
        }
        $nbPages = ceil($nbCook/ $nbByPage);
        if(isset($_GET['page']) && $_GET['page'] > 0 && $currentPage <= $nbPages){
            $currentPage = $_GET['page'];
        } else {
            $currentPage = 1;
        }
        $cooks = $manager->getAll($nbByPage, $currentPage);
        $view = new View('site', 'cooks', 'cooks', 'liste des cuisiniers');
        $view->displayView(array('cooks'=>$cooks, 'nbPages'=>$nbPages, 'currentPage'=>$currentPage));
    }
}
