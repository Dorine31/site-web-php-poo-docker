<?php
namespace controller\site;
use classes\View;
use model\site as ms;

class Home {
    public function displayHome(){
        $recipeManager = new ms\RecipeManager();
        #define and create pages according to the number of recipes to display
        $nbByPage= 10;
        $currentPage = 1;
        if(isset($_SESSION['login'])){
            $counter = $recipeManager::getTotalNber();
        } else {
            $counter = $recipeManager::getTotalNberVisible();
        }
        foreach ($counter as $row){
            $nbRecipe = $row->total;
        }
        $nbPages = ceil($nbRecipe/ $nbByPage);

        if(isset($_GET['page']) && $_GET['page'] > 0 && $currentPage <= $nbPages){
            $currentPage = $_GET['page'];
        } else {
            $currentPage = 1;
        }
        if(isset($_SESSION['login'])){
            $total = $recipeManager->readAll($nbByPage, $currentPage);
        } else {
            $total = $recipeManager->readAllVisible($nbByPage, $currentPage);
        }
        $view = new View('site', 'home', 'Recettes', 'liste des recettes');
        $view->displayView(array('total'=>$total, 'nbPages'=>$nbPages, 'currentPage'=>$currentPage));
    }
}
