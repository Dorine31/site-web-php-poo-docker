<?php
namespace model;
use \PDO;
use \PDOException;

# connexion to the database and requests prepare
class Manager {

    private $dbname;
    private $port;
    private $charset;
    private $user;
    private $pass;
    private $host;
    private $pdo;

    public function __construct($dbname, $host, $user, $pass, $port = 3306, $charset = 'utf8')
    {
        $this->dbname = $dbname;
        $this->port = $port;
        $this->charset = $charset;
        $this->user = $user;
        $this->pass = $pass;
        $this->host = $host;
    }

    private function getPDO()
    {
        $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname . ';port=' . $this->port . ';charset=' . $this->charset;
        if ($this->pdo === null) {
            try {
                $PDO = new PDO($dsn, $this->user, $this->pass);
                $PDO->exec('set names utf8');
                $PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $this->pdo = $PDO;
            } catch (PDOException $e) {
                die("erreur getPDO" . $e->getMessage());
            }
        }
        return $this->pdo;
    }

    public function select($statement, $class)
    {
        $req = $this->getPDO()->query($statement);
        $datas = $req->fetchAll(PDO::FETCH_CLASS, $class);
        return $datas;
    }

    public function filter($filters)
    {
        # data quality
        $result = filter_input_array(INPUT_POST, $filters);
        $emptyArray = 0;
        $falseArray = 0;
        foreach ($filters as $key => $value) {
            if (empty($_POST[$key])) {
                echo '<div class="result">' . $key . ' est vide</div>';
                $emptyArray++;
            } else if ($result[$key] === false) {
                echo '<div class="result">' . $key . ' est non valide</div>';
                $falseArray++;
            }
        };

        return [$result, $emptyArray, $falseArray];
    }

    public function prepareSelect($statement, $attributes, $class, $one = false)
    {
        try {
            # data are sanitized
            $prep = $this->getPDO()->prepare($statement);
            # bindvalues
            foreach ($attributes as $key => $value) {
                if (is_string($value)) {
                    $pdo_param = PDO::PARAM_STR;
                } else {
                    $pdo_param = PDO::PARAM_INT;
                }
                $prep->bindValue($key + 1, $value, $pdo_param);
            }
            $prep->execute();
            $prep->setFetchMode(PDO::FETCH_CLASS, $class);
            if ($one) {
                $res = $prep->fetch();
            } else {
                $res = $prep->fetchAll();
            }
            return $res;
        } catch (PDOException $e) {
            echo 'Erreur prepareSelect' . $e;
        }
    }

    public function prepareInsert($statement, $attributes)
    {
        try {
            $prep = $this->getPDO()->prepare($statement);
            # bindvalues
            foreach ($attributes as $key => $value) {
                if (is_string($value)) {
                    $pdo_param = PDO::PARAM_STR;
                } else {
                    $pdo_param = PDO::PARAM_INT;
                }
                $prep->bindValue($key+1, $value, $pdo_param);
            }
            $prep->execute();
            return $prep;
        } catch (PDOException $e) {
            echo 'Erreur prepareInsert' . $e;
        }
    }

    public function prepareDeleteAll($statement)
    {
        try {
            $prep = $this->getPDO()->prepare($statement);
            $prep->execute();
            return $prep->rowCount();
        } catch (PDOException $e) {
            echo 'Erreur prepareDeleteAll' . $e;
        }
    }


    public function prepareDelete($statement, $attributes)
    {
        try {
            $prep = $this->getPDO()->prepare($statement);
            # bindvalues
            foreach ($attributes as $key => $value) {
                if (is_string($value)) {
                    $pdo_param = PDO::PARAM_STR;
                } else {
                    $pdo_param = PDO::PARAM_INT;
                }
                $prep->bindValue($key + 1, $value, $pdo_param);
            }
            $prep->execute();
            return $prep->rowCount();
        } catch (PDOException $e) {
            echo 'Erreur prepareDelete' . $e;
        }
    }


    public function prepareUpdate($statement, $attributes)
    {
        try {
            # data are sanitized
            $prep = $this->getPDO()->prepare($statement);
            # bindvalues
            foreach ($attributes as $key => $value) {
                if (is_string($value)) {
                    $pdo_param = PDO::PARAM_STR;
                } else {
                    $pdo_param = PDO::PARAM_INT;
                }
                $prep->bindValue($key+1, $value, $pdo_param);
            }
            $prep->execute($attributes);
            return $prep;
        } catch (PDOException $e) {
            echo '<div class="result">Erreur' .$e;'</div>';
        }
    }
}