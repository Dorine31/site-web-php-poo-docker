<?php

namespace model\admin;
use classes\Cnx;

class AuthManager {

    #CRUD
    # read
    public static function find($login, $password){
        $result = Cnx::getCnx()->prepareSelect('SELECT * FROM user WHERE emailUser LIKE ? AND pswdUser LIKE ?;', [$login, $password], get_called_class());
        foreach($result as $data){
            $user = new Auth();
            $user->setIdUser($data->idUser);
            $user->setEmailUser($data->emailUser);
            $user->setPswdUser($data->pswdUser);
        }
        return $user;
    }

    public function getMsgCreateAuth(){
        return '<i>* Tous les champs sont obligatoires.</i>';
    }
    public function getMsgErrorAuth(){
        return '<i style="color:red;">Le login ou le mot de passe est incorrect.</i>';
    }
/*
    public function __get($key){
        $method = 'get' .ucfirst($key);
        $this->$key = $this->$method();
        return $this->$key;
    }

    public function getUrl(){
        return 'index.php?p=auth';
    }*/
}