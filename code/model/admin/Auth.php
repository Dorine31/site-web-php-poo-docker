<?php
namespace model\admin;

class Auth {
    private $idUser;
    private $emailUser;
    private $pswdUser;

    #getters
    public function getIdUser(){
        return $this->idUser;
    }
    public function getEmailUser(){
        return $this->emailUser;
    }
    public function getPswdUser(){
        return $this->pswdUser;
    }

    #setters
    public function setIdUser($idUser){
        if($idUser > 0){
            $this->idUser = $idUser;
        }
    }
    public function setEmailUser($emailUser){
        if(is_string($emailUser)){
            $this->emailUser = $emailUser;
        }
    }
    public function setPswdUser($pswdUser){
        if(is_string($pswdUser)){
            $this->pswdUser = $pswdUser;
        }
    }
}