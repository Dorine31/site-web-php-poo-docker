<?php
namespace model\site;

class Cook {
    private $idCook;
    private $nameCook;
    private $firstnameCook;
    private $photoCook;

    # getters
    public function getIdCook(){
        return $this->idCook;
    }
    public function getNameCook(){
        return $this->nameCook;
    }
    public function getFirstnameCook(){
        return $this->firstnameCook;
    }
    public function getPhotoCook(){
        return $this->photoCook;
    }

    # setters
    public function setIdCook($idCook){
        if($idCook > 0){
            $this->idCook = $idCook;
        }
    }
    public function setNameCook($nameCook){
        if(is_string($nameCook)){
            $this->nameCook = $nameCook;
        }
    }
    public function setFirstnameCook($firstnameCook){
        if(is_string($firstnameCook)){
            $this->firstnameCook = $firstnameCook;
        }
    }
    public function setPhotoCook($photoCook){
        if(is_string($photoCook)){
            $this->photoCook = $photoCook;
        }
    }
    public static function dispatchCook($result){
        $cook = new Cook();
        $cook->setIdCook($result->idCook);
        $cook->setFirstnameCook($result->firstnameCook);
        $cook->setNameCook($result->nameCook);
        $cook->setPhotoCook($result->photoCook);
        return $cook;
    }
}
