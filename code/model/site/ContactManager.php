<?php

namespace model\site;
use classes\Cnx;

class ContactManager {
    # data quality
    public static function filter(){
        $filters = array(
            'firstname' => FILTER_SANITIZE_STRING,
            'lastname' => FILTER_SANITIZE_STRING,
            'email' => FILTER_VALIDATE_EMAIL,
            'object' => FILTER_SANITIZE_STRING
        );
        return Cnx::getCnx()->filter($filters);
    }

    # CRUD
    # Create
    public static function createContact($arr){
        $result = $arr[0];
        $emptyArray = $arr[1];
        $falseArray = $arr[2];
        if($emptyArray == 0 && $falseArray == 0) {
            $newArray = array(
                $result['lastname'],
                $result['firstname'],
                $result['email'],
                $result['object']);
            $sql = 'INSERT INTO Contact (nameContact, firstnameContact, emailContact, objectContact) VALUES (?, ?, ?, ?);';
            return Cnx::getCnx()->prepareInsert($sql, $newArray);
        }
    }

    public function __get($key){
        $method = 'get' .ucfirst($key);
        $this->$key = $this->$method();
        return $this->$key;
    }
    /*
        public function getUrl(){
            return 'index.php?p=contact';
        }*/
    public function getMsgCreateContact(){
        return '<i>* Tous les champs sont obligatoires.</i>';
    }

    public function getMsgSuccessContact(){
        return '<i style="color:forestgreen;">Contact envoyé dans la base de données.</i>';
    }
}

