<?php

namespace model\site;
use classes\Cnx;

class RecipeManager {

    # data quality
    public static function filter(){
        $filters = array(
            'titleRecipe' => FILTER_SANITIZE_STRING,
            'photoRecipe' => FILTER_SANITIZE_STRING,
            'difficulty' => FILTER_SANITIZE_NUMBER_INT,
            'duration' => FILTER_SANITIZE_NUMBER_INT,
            'cost' => FILTER_SANITIZE_NUMBER_INT,
            'description' => FILTER_SANITIZE_STRING,
            'steps' => FILTER_SANITIZE_STRING,
            'personsNumber' => FILTER_SANITIZE_NUMBER_INT,
            'idCook' => FILTER_SANITIZE_NUMBER_INT,
            'ingredients' => FILTER_SANITIZE_STRING
        );
        return Cnx::getCnx()->filter($filters);
    }

    #CRUD
    # CREATE
    public static function insert($arr){
        $result = $arr[0];
        $emptyArray = $arr[1];
        $falseArray = $arr[2];
        if($emptyArray == 0 && $falseArray == 0) {
            $newArray = array(
                $result['titleRecipe'],
                $result['photoRecipe'],
                $result['difficulty'],
                $result['duration'],
                $result['cost'],
                $result['description'],
                $result['steps'],
                $result['personsNumber'],
                $_POST['visibility'],
                $result['idCook'],
                $result['ingredients']);

            $sql = 'INSERT INTO Recipe (titleRecipe, photoRecipe,
                difficulty, duration, cost, description, steps,
                personsNumber, visibility, idCook, ingredients)
                values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
            ';
            $result = Cnx::getCnx()->prepareInsert($sql, $newArray);
            return Recipe::dispatchRecipe($result);
        }
    }

    # READ
    public static function readOne($id){
        $result = Cnx::getCnx()->prepareSelect('SELECT * FROM Recipe, Cook WHERE Recipe.idCook = Cook.idCook AND idRecipe = ?;', [$id], get_called_class(), true);
        return Recipe::dispatchRecipe($result);
    }


    public static function readAll($nbByPage, $currentPage){
        return Cnx::getCnx()->select('SELECT * FROM Recipe, Cook WHERE Recipe.idCook = Cook.idCook ORDER BY titleRecipe LIMIT '.(($currentPage - 1) * $nbByPage).','. $nbByPage.';', get_called_class());
    }

    public static function readAllVisible($nbByPage, $currentPage){
        return Cnx::getCnx()->select('SELECT * FROM Recipe, Cook WHERE Recipe.idCook = Cook.idCook AND visibility = 1 ORDER BY titleRecipe LIMIT '.(($currentPage - 1) * $nbByPage).','. $nbByPage.';', get_called_class());
    }

    # UPDATE
    public static function update($arr, $id){
        $result = $arr[0];
        $emptyArray = $arr[1];
        $falseArray = $arr[2];
        if($emptyArray == 0 && $falseArray == 0 && !empty($id)) {
            $newArray = array($result['titleRecipe'],
                $result['photoRecipe'],
                $result['difficulty'],
                $result['duration'],
                $result['cost'],
                $result['description'],
                $result['steps'],
                $result['personsNumber'],
                $_POST['visibility'],
                $result['idCook'],
                $result['ingredients'],
                $id);

            $sql = 'UPDATE Recipe SET titleRecipe= ?,
                photoRecipe=?,
                difficulty=?,
                duration=?,
                cost=?,
                description=?,
                steps=?,
                personsNumber=?,
                visibility=?,
                idCook=?,
                ingredients=?
                WHERE idRecipe = ?;
            ';
            $result = Cnx::getCnx()->prepareUpdate($sql, $newArray);
            return Recipe::dispatchRecipe($result);
        }
    }

    # DELETE
    public static function deleteAllRecipe(){
        return Cnx::getCnx()->prepareDeleteAll('DELETE FROM Recipe;');
    }

    public static function deleteOne($id){
        $result = Cnx::getCnx()->prepareDelete('DELETE FROM Recipe WHERE idRecipe = ?;', [$id]);
        return Recipe::dispatchRecipe($result);
    }

    #messages
    public function getMsgCreateRecipe(){
        return '<i>* Tous les champs sont obligatoires.</i>';
    }
    public function getMsgErrorRecipe(){
        return '<i style="color: #ff0000;"> Veuillez remplir tous les champs.</i>';
    }
    public function getMsgSuccessInsertRecipe(){
        return '<i style="color: forestgreen;">La recette a été insérée dans la base de données.</i>';
    }
    public function getMsgSuccessModifyRecipe(){
        return '<i style="color: forestgreen;">La recette a été modifiée.</i>';
    }
    public function getMsgSuccessDeleteRecipe(){
        return '<i style="color: forestgreen;">La recette a été supprimée.</i>';
    }
        public function getMsgSuccessDeleteAllRecipe(){
        return '<i style="color: forestgreen;">Les recettes ont été supprimées.</i>';
    }


    # COUNT FOR PAGES
    public static function getTotalNber(){
        return Cnx::getCnx()->select('SELECT count(*) AS total FROM Recipe;', get_called_class());
    }
    public static function getTotalNberVisible(){
        return Cnx::getCnx()->select('SELECT count(*) AS total FROM Recipe WHERE visibility = 1;', get_called_class());
    }

    public function getNbRecipes(){
        return $this->total;
    }
}