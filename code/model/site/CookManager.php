<?php
namespace model\site;
use classes\Cnx;

class CookManager {
    # data quality
    public static function filter(){
        $filters = array(
            'lastname' => FILTER_SANITIZE_STRING,
            'firstname' => FILTER_SANITIZE_STRING,
            'photoCook' => FILTER_SANITIZE_STRING,
        );
        return Cnx::getCnx()->filter($filters);
    }

    # CRUD
    # CREATE
    public static function insert($arr){
        $result = $arr[0];
        $emptyArray = $arr[1];
        $falseArray = $arr[2];
        if($emptyArray == 0 && $falseArray == 0) {
            $newArray = array(
                $result['lastname'],
                $result['firstname'],
                $result['photoCook']);

            $sql = 'INSERT INTO Cook (nameCook, firstnameCook, photoCook)
                values (?, ?, ?);
            ';
            $result = Cnx::getCnx()->prepareInsert($sql, $newArray);
            return Cook::dispatchCook($result);
        }
    }

    # READ
    public static function find($id){
        $result = Cnx::getCnx()->prepareSelect('select * FROM Cook WHERE idCook = ?',[$id], get_called_class(), true);
        return Cook::dispatchCook($result);
    }

    public static function getAll($nbByPage, $currentPage){
        $resultArray = Cnx::getCnx()->select('SELECT * FROM Cook ORDER BY nameCook LIMIT '.(($currentPage - 1) * $nbByPage).','. $nbByPage.';', get_called_class());
        foreach ($resultArray as $result){
            $cook = Cook::dispatchCook($result);
            $cooks[] = $cook;
        }
        return $cooks;
    }

    # UPDATE
    public static function update($arr, $id){
        $result = $arr[0];
        $emptyArray = $arr[1];
        $falseArray = $arr[2];
        if($emptyArray == 0 && $falseArray == 0 && !empty($id)) {
            $newArray = array($result['lastname'], $result['firstname'], $result['photoCook'], $id);
            $sql = 'UPDATE Cook SET nameCook= ?,
                firstnameCook= ?,
                photoCook= ?
                WHERE idCook = ?;
            ';
            $result = Cnx::getCnx()->prepareUpdate($sql, $newArray);
            return Cook::dispatchCook($result);
        }
    }

/*
    public static function modifyCook($data){
        return Cnx::getCnx()->prepareModify('UPDATE FROM Cook WHERE idCook = ?;', [$data]);
    }*/

    # DELETE
    public static function deleteOne($id){
        $result = Cnx::getCnx()->prepareDelete('DELETE FROM Cook WHERE idCook = ?;', [$id]);
        return Cook::dispatchCook($result);
    }

    public static function deleteAllCook(){
        return Cnx::getCnx()->prepareDeleteAll('DELETE FROM Cook;');
    }

    # pages
    public static function getTotalNber(){
        return Cnx::getCnx()->select('SELECT count(*) AS total FROM Cook;', get_called_class());
    }

    #messages
    public function getMsgCreateCook(){
        return '<i>* Tous les champs sont obligatoires.</i>';
    }
    public function getMsgErrorCook(){
        return '<i style="color: #ff0000;"> Veuillez remplir tous les champs.</i>';
    }
    public function getMsgSuccessInsertCook(){
        return '<i style="color: forestgreen;">Le cuisinier a été inséré dans la base de données.</i>';
    }
    public function getMsgSuccessModifyCook(){
        return '<i style="color: forestgreen;">Le cuisinier a été modifié.</i>';
    }
    public function getMsgSuccessDeleteCook(){
        return '<i style="color: forestgreen;">Le cuisinier a été modifié avec toutes les recettes associées.</i>';
    }
    public function getMsgSuccessDeleteAllCooks(){
        return '<i style="color: forestgreen;">Les cuisiniers ont été supprimés avec toutes les recettes associées.</i>';
    }
/*
    public function __get($key){
        $method = 'get' .ucfirst($key);
        $this->$key = $this->$method();
        return $this->$key;
    }

    public function getUrl(){
        return 'index.php?p=cook&id=' .$this->idCook;
    }

    public function getUrlDelete(){
        return 'index.php?p=deleteCook&id=' .$this->idCook;
    }

    public function getUrlModify(){
        return 'index.php?p=modifyCook&id=' .$this->idCook;
    }
*/
    public function getNbCooks(){
        return $this->total;
    }

    public function getContent(){
        $content = "<img class='img-cook' src=\"http://cuisine.local:8080/" . $this->photoCook . "\">";
        $content .= '<div class="wrapper-name-cook"><span>'.$this->firstnameCook.'</span>';
        $content .= ' ';
        $content .= '<span>'.$this->nameCook.'</span></div>';
        return $content;
    }
}