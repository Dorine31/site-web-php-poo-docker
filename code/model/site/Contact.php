<?php
namespace model\site;

class Contact {
    private $idContact;
    private $nameContact;
    private $firstnameContact;
    private $emailContact;
    private $objectContact;

    # getters
    public function getIdContact(){
        return $this->idContact;
    }
    public function getNameContact(){
        return $this->nameContact;
    }
    public function getFirstnameContact(){
        return $this->firstnameContact;
    }
    public function getEmailContact(){
        return $this->$emailContact;
    }
    public function getObjectContact(){
        return $this->$objectContact;
    }

    # setters
    public function setIdContact($idContact){
        if($idContact > 0){
            $this->idContact = $idContact;
        }
    }
    public function setNameContact($nameContact){
        if(is_string($nameContact)){
            $this->nameContact = $nameContact;
        }
    }
    public function setFirstnameContact($firstnameContact){
        if(is_string($firstnameContact)){
            $this->firstnameContact = $firstnameContact;
        }
    }
    public function setEmailContact($emailContact){
        if(is_string($emailContact)){
            $this->emailContact = $emailContact;
        }
    }
    public function setObjectContact($objectContact){
        if(is_string($objectContact)){
            $this->objectContact = $objectContact;
        }
    }
}