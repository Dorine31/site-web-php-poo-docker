<?php
namespace model\site;

class Recipe {
    private $idRecipe;
    private $titleRecipe;
    private $photoRecipe;
    private $difficulty;
    private $duration;
    private $cost;
    private $description;
    private $steps;
    private $personsNumber;
    private $visibility;
    private $idCook;
    private $ingredients;

    # getters
    public function getIdRecipe(){
        return $this->idRecipe;
    }
    public function getTitleRecipe(){
        return $this->titleRecipe;
    }
    public function getPhotoRecipe(){
        return $this->photoRecipe;
    }
    public function getDifficulty(){
        return $this->difficulty;
    }
    public function getDuration(){
       return $this->duration;
    }
    public function getCost(){
       return $this->cost;
    }
    public function getDescription(){
        return $this->description;
    }
    public function getSteps(){
        return $this->steps;
    }
    public function getPersonsNumber(){
        return $this->personsNumber;
    }
    public function getVisibility(){
        return $this->visibility;
    }
    public function getIdCookRecipe(){
        return $this->idCook;
    }
    public function getIngredients(){
        return $this->ingredients;
    }

    # setters
    public function setIdRecipe($idRecipe){
        if($idRecipe > 0){
            $this->idRecipe = $idRecipe;
        }
    }
    public function setTitleRecipe($titleRecipe){
        if(is_string($titleRecipe)){
            $this->titleRecipe = $titleRecipe;
        }
    }
    public function setPhotoRecipe($photoRecipe){
        if(is_string($photoRecipe)){
            $this->photoRecipe = $photoRecipe;
        }
    }
    public function setDifficulty($difficulty){
        if($difficulty > 0){
            $this->difficulty = $difficulty;
        }
    }
    public function setDuration($duration){
        if($duration > 0){
            $this->duration = $duration;
        }
    }
    public function setCost($cost){
        if($cost > 0){
            $this->cost = $cost;
        }
    }
    public function setDescription($description){
        if(is_string($description)){
            $this->description = $description;
        }
    }
    public function setSteps($steps){
        if(is_string($steps)){
            $this->steps = $steps;
        }
    }
    public function setPersonsNumber($personsNumber){
        if($personsNumber > 0){
            $this->personsNumber = $personsNumber;
        }
    }
    public function setVisibility($visibility){
        if($visibility >= 0 && $visibility <= 1){
            $this->visibility = $visibility;
        }
    }
    public function setIdCook($idCook){
        if($idCook > 0){
            $this->idCook = $idCook;
        }
    }
    public function setIngredients($ingredients){
        if(is_string($ingredients)){
            $this->ingredients = $ingredients;
        }
    }
    public static function dispatchRecipe($result){
        $recipe = new Recipe();
        $recipe->setIdRecipe($result->idRecipe);
        $recipe->setTitleRecipe($result->titleRecipe);
        $recipe->setPhotoRecipe($result->photoRecipe);
        $recipe->setDifficulty($result->difficulty);
        $recipe->setDuration($result->duration);
        $recipe->setCost($result->cost);
        $recipe->setDescription($result->description);
        $recipe->setSteps($result->steps);
        $recipe->setPersonsNumber($result->personsNumber);
        $recipe->setVisibility($result->visibility);
        $recipe->setIdCook($result->idCook);
        $recipe->setIngredients($result->ingredients);
        return $recipe;
    }
}