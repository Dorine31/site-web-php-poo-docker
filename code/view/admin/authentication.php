    <h2>Connexion</h2>
    <div class="result"><?= $msg; ?></div>
    <form method="POST" action="index.php?p=auth" id="form" class="needs-validation center-div" novalidate>
        <div class="form-group row mt-sm-2">
            <input type="email" id="login" class="form-control" name="login" maxlength="255" placeholder="Adresse email" required>
            <div class="invalid-feedback">
                Entrer une adresse mail valide.
            </div>
        </div>
        <div class="form-group row mt-sm-2">
            <input type="password" id="password" class="form-control" name="password" pattern=".{6,}"  maxlength="50" placeholder="Mot de passe" required>
            <div class="invalid-feedback">
                Saisir un mot de passe.
            </div>
        </div>
        <div id="pad" class="center-div"></div>
        <div class="submit-reset">
            <button type="button" id="reset" class="btn btn-submit mt-sm-2">Supprimer</button>
            <button type="submit" id="submit" class="btn btn-submit mt-sm-2" >Valider</button>
        </div>
    </form>
<script src="../public/js/keyboard.js"></script>