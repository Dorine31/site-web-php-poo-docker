<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <link rel="stylesheet" href="<?= PUBLIQUE; ?>/css/styles.css">
    <title><?= $title; ?></title>
</head>
<body>
<header>
    <h1>Les magiciens du fouet</h1>
</header>

<!--Navbar-->
<nav id="navbar" class="navbar navbar-expand-md">
    <button class="navbar-toggler navbar-btn navbar-dark bg-dark" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon text-dark"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarContent">
        <ul class="navbar-nav w-100">
            <?php
            if (isset($_SESSION['login'])) {
            ?>
            <li class="nav-item">
                <a class="nav-link" href="index.php?p=user">Tableau de bord</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="index.php?p=addRecipe">Ajouter une recette</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="index.php?p=addCook">Ajouter un cuisinier</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="index.php?p=deconnection">Déconnexion</a>
            </li>

<?php
} else {
?>
<li class="nav-item">
    <a class="nav-link" href="index.php">Home</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="index.php?p=contact">Contact</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="index.php?p=auth">Connexion</a>
</li>
    <?php
    }
    ?>
        </ul>
    </div>
</nav>
<?php echo '<div class="breadcrumb"> Vous êtes ici : '.$breadcrumb. '</div>' ?>
<div class="content">
<?= $content; ?>
</div>
<footer class="footer">
    <div class="mention">CDA 2021 - Développement Dorine BERTON</div>
</footer>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="<?= PUBLIQUE; ?>/js/validation.js"></script>
<script src="<?= PUBLIQUE; ?>/js/sticky.js"></script>
</body>
</html>