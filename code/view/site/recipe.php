<h2><?= $recipe->getTitleRecipe(); ?></h2>
<div class="content-single-recipe">
<img class='img-recipe' src="<?= PUBLIQUE.'/'.$recipe->getPhotoRecipe(); ?>" >
<div>
<div><?= $recipe->getDescription(); ?></div>
<div><?php
    $cost = $recipe->getCost();
    if(isset($cost)){
        for ($j = 1; $j <= $cost; $j++) {
            ?>
            <span class='span-round'>€</span>
            <?php
        }
    }
#display difficulty
$difficulty = $recipe->getDifficulty();
if(isset($difficulty)){
    for ($i = 1; $i <= $difficulty; $i++) {
        ?>
        <span class='span-round'><img class='fouet' src="<?= PUBLIQUE; ?>/img/fouet.png"></span>
        <?php
    }
}
    # convert duration into heures, minutes and secondes
    $duration = $recipe->getDuration();
    if(isset($duration)) {
        $heures = $minutes = 0;
        if ($duration > 59) {
            $minutes = intval($duration / 60);
        }
        if ($duration > 3599) {
            $heures = intval($duration / 3600);
            $minutes = (fmod($duration, 3600) / 60);
        }
        if (!empty($heures) && !empty($minutes)) {
            ?>
            <span><?= $heures; ?> heures <span><?= $minutes; ?> minutes</span>
    <?php
        } else if (!empty($heures)) {
            ?>
            <span><?= $heures; ?> heures</span>
    <?php
        } else if (!empty($minutes)) {
            ?>
            <span><?= $minutes; ?> minutes</span>
    <?php
        };
    }
?></div>
<div>Nombre de personnes : <?= $recipe->getPersonsNumber(); ?></div>
</div>
</div>
    <div class="wrapper-single-recipe">
        <h3>Etapes</h3>
        <div><?php
            $str = str_replace('&#39;', '\'', $recipe->getSteps());
            $steps = explode(';', $str);
            #$steps = explode(';', $recipe->getSteps());
            ?>
            <ol>
            <?php
            foreach ($steps as $step){
            ?>
            <li><?= $step; ?></li>
            <?php
            }
            ?>
            </ol>
        </div>
        <h3>Ingrédients</h3>
        <div><?php
            $str = str_replace('&#39;', '\'', $recipe->getIngredients());
            $ingredients = explode(';', $str);
            ?>
            <ul>
            <?php
            foreach ($ingredients as $ingredient){
            ?>
            <li><?= $ingredient; ?></li>
            <?php
            }
            ?>
            </ul>
        </div>
    </div>
<?php
if(isset($_SESSION['login'])){
    ?>
    <div class="list-btn">
    <button class="btn btn-modify modify"><a href="index.php?p=modifyRecipe&id=<?= $recipe->getIdRecipe(); ?>">Modifier</a></button>
    <button class="btn btn-modify delete"><a href="index.php?p=deleteRecipe&id=<?= $recipe->getIdRecipe(); ?>">Supprimer</a></button>
    </div>
    <?php
}
?>
