<div class="result"><?= $msg; ?></div>
<form method="POST" id="form" class="needs-validation center-div" novalidate>
    <div class="form-group row mt-sm-2">
        <label>Titre de la recette</label>
        <input type="text" id="titleRecipe" name="titleRecipe" class="form-control" value="<?= $recipe->getTitleRecipe()?>"/>
    </div>
    <div class="form-group row mt-sm-2">
        <label>Photo</label>
        <input type="text" id="photoRecipe" name="photoRecipe" class="form-control" value="<?= $recipe->getPhotoRecipe()?>"/>
    </div>
    <div class="form-group row mt-sm-2">
        <label>Difficulté</label>
        <input type="number" id="difficulty" name="difficulty" class="form-control" placeholder="difficulté" value="<?= $recipe->getDifficulty()?>"/>
    </div>
    <div class="form-group row mt-sm-2">
        <label>Durée</label>
        <input type="number" id="duration" name="duration" class="form-control" value="<?= $recipe->getDuration()?>"/>
    </div>
    <div class="form-group row mt-sm-2">
        <label>Coût</label>
        <input type="number" id="cost" name="cost" class="form-control" value="<?= $recipe->getCost()?>"/>
    </div>
    <div class="form-group row mt-sm-2">
        <label>Description</label>
        <textarea id="description" name="description" class="form-control" rows="3"><?= $recipe->getDescription()?></textarea>
    </div>
    <div class="form-group row mt-sm-2">
        <label>Etapes</label>
        <textarea id="steps" name="steps" class="form-control" rows="10"><?= $recipe->getSteps()?></textarea>
    </div>
    <div class="form-group row mt-sm-2">
        <label>Nombre de personnes</label>
        <input type="number" id="personsNumber" name="personsNumber" class="form-control" value="<?= $recipe->getPersonsNumber()?>"/>
    </div>
    <div class="form-group row mt-sm-2">
        <label>Visibilité</label>
        <input type="number" id="visibility" name="visibility" class="form-control" min="0" max="1" value="<?= $recipe->getVisibility()?>"/>
    </div>
    <div class="form-group row mt-sm-2">
        <label>Id du cuisinier qui a élaboré la recette</label>
        <input type="number" id="idCook" name="idCook" class="form-control" value="<?= $recipe->getIdCookRecipe()?>"/>
    </div>
    <div class="form-group row mt-sm-2">
        <label>Ingrédients</label>
        <textarea id="ingredients" name="ingredients" class="form-control" rows="3"><?= $recipe->getIngredients()?></textarea>
    </div>
    <button type="submit" id="submit" class="btn btn-submit mt-sm-2">Valider</button>
</form>
<button class="btn btn-modify back"><a href="index.php?p=home">Liste des recettes</a></button>