<h2>supprimer la recette <?= $recipe->getIdRecipe(); ?></h2>
<div id="form-delete">
    <h4><?= $recipe->getTitleRecipe(); ?></h4>
    <?php
    if(!isset($msg)){
    ?>
    <div>Etes-vous certain de vouloir supprimer cette recette ?</div>
    <form action="" method="post">
        <input type="submit" name="non" value="Non">
        <input type="hidden" name="idRecipe" value="<?= $recipe->getIdRecipe(); ?>">
        <input type="submit" name="oui" value="Oui">
    </form>

<?php
} else { echo $msg; }
 ?>
 </div>
<button class="btn btn-modify back"><a href="index.php?p=home">Liste des recettes</a></button>