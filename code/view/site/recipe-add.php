    <h2>Ajouter une recette</h2>
    <div class="result"><?= $msg; ?></div>
    <form id="addRecipe" action="" method="POST" class="needs-validation center-div" novalidate">
    <div class="form-group row mt-sm-2">
        <input type="text" class="form-control" id="titleRecipe" name="titleRecipe" placeholder="Nom de la recette" required>
        <div class="invalid-feedback">
            Remplir le champ.
        </div>
    </div>
    <div class="form-group row mt-sm-2">
        <input type="text" class="form-control" id="photoRecipe" name="photoRecipe" placeholder="Lien vers la photo de la recette" required>
        <div class="invalid-feedback">
            Ajouter le lien vers une photo.
        </div>
    </div>
    <div class="form-group row mt-sm-2">
        <input type="number" class="form-control" id="difficulty" name="difficulty" placeholder="Difficulté de 1 à 4" min="1" max="4" required>
        <div class="invalid-feedback">
            Remplir le champ.
        </div>
    </div>
    <div class="form-group row mt-sm-2">
        <input type="number" class="form-control" id="duration" name="duration" placeholder="Temps de réalisation en secondes" required>
        <div class="invalid-feedback">
            Remplir le champ.
        </div>
    </div>
    <div class="form-group row mt-sm-2">
        <input type="number" class="form-control" id="cost" name="cost" placeholder="Coût" min="1" max="4" required>
        <div class="invalid-feedback">
            Remplir le champ.
        </div>
    </div>
    <div class="form-group row mt-sm-2">
        <input type="text" class="form-control" id="description" name="description" placeholder="Description" required>
        <div class="invalid-feedback">
            Remplir le champ.
        </div>
    </div>
    <div class="form-group row mt-sm-2">
        <textarea type="text" class="form-control" id="steps" name="steps" placeholder="Etapes" rows="10" required></textarea>
        <div class="invalid-feedback">
            Remplir le champ.
        </div>
    </div>
    <div class="form-group row mt-sm-2">
        <input type="number" class="form-control" id="personsNumber" name="personsNumber" placeholder="Nombre de personnes" required>
        <div class="invalid-feedback">
            Remplir le champ.
        </div>
    </div>
    <div class="form-group row mt-sm-2">
        <Input type="number" class="form-control" id="visibility" name="visibility" min="0" max="1" placeholder="Visibilité" required>
        <div class="invalid-feedback">
            Remplir le champ.
        </div>
    </div>
    <div class="form-group row mt-sm-2">
        <!--<Input type="text" class="form-control" id="idCook" name="idCook" placeholder="Id du cuisinier" required>-->
        <select name="idCook" id="idCook">
            <option style="width:300px;" value="">--Choisir un id de cuisinier--</option>
            <?php
                foreach($cooks as $cook){
            ?>
            <option style="width:300px;" value="<?= $cook->getIdCook(); ?>"><?= $cook->getIdCook(); ?></option>
            <?php
            } 
            ?>
        </select>
    </div>
    <div class="form-group row mt-sm-2">
        <textarea type="text" class="form-control" id="ingredients" name="ingredients" placeholder="Ingredients" required></textarea>
        <div class="invalid-feedback">
            Remplir le champ.
        </div>
    </div>
    <button type="submit" class="btn btn-submit mt-sm-2">Valider</button>
    </form>
<button class="btn btn-modify back"><a href="index.php?p=home">Liste des recettes</a></button>