<h2>cuisiniers</h2>
<div class="list-cook">
<?php
if(isset($cooks)){
foreach ($cooks as $cook){
?>
    <div class="wrapper-cook">
        <a href="index.php?p=cook&id=<?= $cook->getIdCook(); ?>" >
            <img class='img-cook' src="<?= PUBLIQUE.'/'.$cook->getPhotoCook(); ?>" >
            <div class='content-cook'>
                <div><?= $cook->getFirstnameCook().' '.$cook->getNameCook(); ?></div>
            </div>
            <div class="list-btn">
                <button class="btn btn-modify modify"><a href="index.php?p=modifyCook&id=<?= $cook->getIdCook(); ?>">Modifier</a></button>
                <button class="btn btn-modify delete"><a href="index.php?p=deleteCook&id=<?= $cook->getIdCook(); ?>">Supprimer</a></button>
            </div>
        </a>
    </div>
<?php
}
?>

</div>
<button class="btn btn-modify deleteAll"><a href="index.php?p=deleteAllCooks">Supprimer tous les cuisiniers</a></button>
<div class="pages page-cooks">
<?php
for($i = 1; $i <= $nbPages; $i++){
        if($i == $currentPage) {
?>
    <div><?= $i ?></div>
<?php
        } else {
?>
    <div><a href="index.php?p=cooks&page=<?= $i; ?> "><?= $i; ?></a></div>

<?php
}}
?>
</div>
<?php
} else {
    ?>
    <div><i  style="color: red;">aucun cuisinier !</i></div>
    <?php
}
