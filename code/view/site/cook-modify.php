<div class="result"><?= $msg; ?></div>
<form method="POST" id="form" class="needs-validation center-div" novalidate>
<div class="form-group row mt-sm-2">
<label>Nom</label>
<input type="text" id="lastname" name="lastname" class="form-control" value="<?= $cook->getNameCook() ?>"/>
</div><div class="form-group row mt-sm-2">
<label>Prénom</label>
<input type="text" id="firstname" name="firstname" class="form-control" value="<?= $cook->getFirstnameCook() ?>"/>
</div><div class="form-group row mt-sm-2">
<label>Photo</label>
<input type="text" id="photoCook" name="photoCook" class="form-control" value="<?= $cook->getPhotoCook() ?>"/>
</div>
<button type="submit" id="submit" class="btn btn-submit mt-sm-2">Valider</button>
</form>
<button class="btn btn-modify back"><a href="index.php?p=cooks">Liste des cuisiniers</a></button>
