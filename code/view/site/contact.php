<h2>Contact</h2>
<div class="result"><?= $msg; ?></div>
<form id="contact" action="" method="POST" class="needs-validation center-div" novalidate>
    <div class="form-group row mt-sm-2">
        <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Nom" required>
        <div class="invalid-feedback">
            Remplir ce champ.
        </div>
    </div>
    <div class="form-group row mt-sm-2">
        <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Prénom" required>
        <div class="invalid-feedback">
            Remplir ce champ.
        </div>
    </div>
    <div class="form-group row mt-sm-2">
        <input type="email" class="form-control" id="email" name="email" placeholder="Adresse email" required>
        <div class="invalid-feedback">
            Entrer une adresse mail valide.
        </div>
    </div>
    <div class="form-group row mt-sm-2">
        <textarea class="form-control" id="object" name="object" placeholder="Objet" rows="5" required></textarea>
    </div>
    <button type="submit" id="submit" class="btn btn-submit mt-sm-2">Valider</button>
</form>