<h2>ajouter un cuisinier</h2>
<div class="result"><?= $msg; ?></div>
<form id="addCook" action="" method="POST" class="needs-validation center-div" novalidate">
<div class="form-group row mt-sm-2">
    <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Nom du cuisinier" required>
    <div class="invalid-feedback">
        Remplir le champ.
    </div>
</div>
<div class="form-group row mt-sm-2">
    <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Prénom du cuisinier" required>
    <div class="invalid-feedback">
        Télécharger une photo.
    </div>
</div>
<div class="form-group row mt-sm-2">
    <input type="text" class="form-control" id="photoCook" name="photoCook" placeholder="Lien vers la photo du cuisinier" required>
    <div class="invalid-feedback">
        Ajouter le lien vers une photo.
    </div>
</div>
<button type="submit" class="btn btn-submit mt-sm-2">Valider</button>
</form>

<button class="btn btn-modify back"><a href="index.php?p=cooks">Liste des cuisiniers</a></button>