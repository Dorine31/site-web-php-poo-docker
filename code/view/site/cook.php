<h2><?= $cook->getNameCook(); ?></h2>
<div class="wrapper-cook wrapper-single-cook">
        <img class='img-cook' src="<?= PUBLIQUE.'/'.$cook->getPhotoCook(); ?>" >
        <div class='content-cook'>
            <div><?= $cook->getFirstnameCook().' '.$cook->getNameCook(); ?></div>
        </div>
        <div class="list-btn">
            <button class="btn btn-modify modify"><a href="index.php?p=modifyCook&id=<?= $cook->getIdCook(); ?>">Modifier</a></button>
            <button class="btn btn-modify delete"><a href="index.php?p=deleteCook&id=<?= $cook->getIdCook(); ?>">Supprimer</a></button>
        </div>
</div>