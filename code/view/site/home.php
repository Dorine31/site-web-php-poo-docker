<h2>Liste des recettes</h2>
<?php
if(isset($total)){
    foreach ($total as $one){
?>
<div class="wrapper-recipe"><a href="index.php?p=recipe&id=<?= $one->idRecipe; ?>" >
<img class='img-recipe' src="<?= PUBLIQUE.'/'.$one->photoRecipe; ?>" >
<div class='content-recipe'>
<div><strong>Titre : </strong><?= $one->titleRecipe; ?>- par <?= $one->firstnameCook.' '.$one->nameCook; ?></div>
<div><?= $one->description; ?></div>
<div><?php
    $cost = $one->cost;
    if(isset($cost)){
        for ($j = 1; $j <= $cost; $j++) {
            ?>
            <span class='span-round'>€</span>
            <?php
        }
    }
#display difficulty
$difficulty = $one->difficulty;
if(isset($difficulty)){
    for ($i = 1; $i <= $difficulty; $i++) {
        ?>
        <span class='span-round'><img class='fouet' src="<?= PUBLIQUE; ?>/img/fouet.png"></span>
        <?php
    }
}
    # convert duration into heures, minutes and secondes
    $duration = $one->duration;
    if(isset($duration)) {
        $heures = $minutes = 0;
        if ($duration > 59) {
            $minutes = intval($duration / 60);
        }
        if ($duration > 3599) {
            $heures = intval($duration / 3600);
            $minutes = (fmod($duration, 3600) / 60);
        }
        if (!empty($heures) && !empty($minutes)) {
            ?>
            <span><?= $heures; ?> heures </span><span><?= $minutes; ?> minutes</span>
    <?php
        } else if (!empty($heures)) {
            ?>
            <span><?= $heures; ?> heure(s)</span>
    <?php
        } else if (!empty($minutes)) {
            ?>
            <span><?= $minutes; ?> minutes</span>
    <?php
        };
    }
?></div>
<div>Nombre de personnes : <?= $one->personsNumber; ?></div>
</div>
</a>
<?php
if(isset($_SESSION['login'])){
    ?>
    <div class="list-btn">
    <button class="btn btn-modify modify"><a href="index.php?p=modifyRecipe&id=<?= $one->idRecipe; ?>">Modifier</a></button>
    <button class="btn btn-modify delete"><a href="index.php?p=deleteRecipe&id=<?= $one->idRecipe; ?>">Supprimer</a></button>
    </div>
    <?php
}
?>
</div>
<?php
}
?>

<button class="btn btn-modify deleteAll"><a href="index.php?p=deleteAllRecipes">Supprimer toutes les recettes</a></button>
<div class="pages page-recipe">
<?php
for($i = 1; $i <= $nbPages; $i++){
        if($i == $currentPage) {
            ?>
<div><?= $i ?></div>
<?php        } else {
        ?>
<div><a href="index.php?p=home&page=<?= $i; ?> "><?= $i; ?></a></div>
<?php
}}
?>
</div>
    <?php
} else {
?>
<div ><i style="color: red;">aucune recette !</i></div>
    <?php
}

