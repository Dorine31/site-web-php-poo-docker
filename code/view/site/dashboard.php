<?php
if(isset($_SESSION['login'])){
    $login = $_SESSION['login'];
?>
<h2>Tableau de bord</h2>
    <div id="wrapper-lists">
        <div id="recipes"><a href="index.php?p=home">Liste des recettes</a></div>
        <div id="cooks"><a href="index.php?p=cooks">Liste des cuisiniers</a></div>
    </div>

<?php
} else {
    header('Location: index.php?p=auth');
}