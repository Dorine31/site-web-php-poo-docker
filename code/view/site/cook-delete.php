<h2>supprimer le cuisinier <?= $cook->getIdCook(); ?></h2>
<div id="form-delete">
<h4><?php $cook->getFirstnameCook(). ' ' .$cook->getNameCook(); ?></h4>
<?php
if(!isset($msg)){
    ?>
    <div>Etes-vous certain de vouloir supprimer ce cuisinier ?</div>
    <form action="" method="post">
        <input type="submit" name="non" value="Non">
        <input type="hidden" name="idRecipe" value="<?= $cook->getIdCook(); ?>">
        <input type="submit" name="oui" value="Oui">
    </form>
    <?php
} else { echo $msg; }
?>
</div>
<button class="btn btn-modify back"><a href="index.php?p=cooks">Liste des cuisiniers</a></button>