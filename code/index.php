<?php
session_start();
require_once '_config.php';

use classes\Autoloader;
Autoloader::autoload($class);
