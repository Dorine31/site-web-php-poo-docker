'use strict';

class Keyboard {

    constructor (div) {
        this.currentDiv = document.getElementById(div);
    }

    setKeyboard () {
        for (let i = 0; i < 16; i++) {
            let newDiv = document.createElement('div');
            newDiv.classList.add('case');
            newDiv.setAttribute('id', 'case' + i)
            this.currentDiv.appendChild(newDiv);
        }
        return this;
    }

    setKeyboardContent () {
        let num, test = true, str = '';
        for (let i = 0; i < 10; i++) {
            while (test == true) {
                num = Math.floor(Math.random() * 16);
                if (str.indexOf('-' + num + '-') > -1) {
                    num = Math.floor(Math.random() * 16);
                } else {
                    let newSpan = document.createElement('span');
                    let newContent = document.createTextNode(i);
                    newSpan.appendChild(newContent);
                    let currentDiv = document.getElementById('case' + num);
                    currentDiv.appendChild(newSpan);
                    document.getElementById('case' + num).setAttribute('data-key', i);
                    str += '-' + num + '-';
                    test = false;
                }
            }
            test = true;
        }
        return this;
    }

    resetKeyboard () {
        let element = this.currentDiv;
        while (element.firstChild) {
            element.removeChild(element.firstChild);
        }
        keyboard.setKeyboard().setKeyboardContent().clickKeyboard();
    }

    clickKeyboard() {
        let form = document.getElementById('form');
        let password = document.getElementById('password');
        let cases = form.getElementsByClassName('case');
        for (let i = 0; i < cases.length; i++) {
            cases[i].addEventListener('click', function (event) {
                password.value = password.value + this.getAttribute('data-key');
            }, false);
        }
    }
}

class Reset {
    constructor (form, login, password, result, reset) {
        this.form = document.getElementById(form);
        this.login = document.getElementById(login);
        this.password = document.getElementById(password);
        this.result = document.getElementById(result);
        this.reset = document.getElementById(reset);
        this.url = this.form.getAttribute('action')
    }

    resetForm () {
        let login = this.login, password = this.password, reset = this.reset;
        reset.addEventListener('click', function (event) {
            login.value = password.value = '';
            keyboard.resetKeyboard();
        }, false);
    }
}

let keyboard = new Keyboard('pad');
keyboard.setKeyboard().setKeyboardContent().clickKeyboard();

let reset = new Reset('form', 'login', 'password', 'result', 'reset');
reset.resetForm();
