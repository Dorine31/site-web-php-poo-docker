<?php
namespace classes;

class View {
    private $folder;
    private $file;
    private $title;
    private $breadcrumb;

    public function __construct($folder, $file, $title, $breadcrumb){
        $this->folder = $folder;
        $this->file = $file;
        $this->title = $title;
        $this->breadcrumb = $breadcrumb;
    }

    public function displayView($resultArray = array()){
        #vérifie que chq clé de la table correspond à une valeur valide
        extract($resultArray);
        $folder = $this->folder;
        $file = $this->file;
        $title = $this->title;
        $breadcrumb = $this->breadcrumb;

        ob_start();
        include(VIEW.'/'.$folder.'/'.$file.'.php');
        $content = ob_get_clean();

        include(VIEW.'/templates/default.php');
}
}