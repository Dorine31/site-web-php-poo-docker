<?php
namespace classes;
/**
 * Class Autoloader
 * @package classes
 */
class Autoloader {
    /**
     * @param $class object permet de récupérer le nom de fichier où se trouve la classe
     */
    static function autoload($class){
        spl_autoload_register(function($class){
        $path = str_replace('\\', '/', $class);
        require_once($path.'.php');
        });

        if(isset($_GET['p'])){
            switch($_GET['p']) {
                case 'home':
                    $namespace = 'controller\site\Home';
                    $methode = 'displayHome';
                    break;
                case 'contact':
                    $namespace = 'controller\site\Contact';
                    $methode = 'createContact';
                    break;
                case 'recipe':
                    $namespace = 'controller\site\Recipe';
                    $methode = 'displayRecipe';
                    break;
                case 'cook':
                    $namespace = 'controller\site\Cook';
                    $methode = 'displayCook';
                    break;
                case 'recipes':
                    $namespace = 'controller\site\Recipes';
                    $methode = 'displayRecipes';
                    break;
                case 'cooks':
                    $namespace = 'controller\site\Cooks';
                    $methode = 'displayCooks';
                    break;
                case 'deleteAllRecipes':
                    $namespace = 'controller\site\RecipeDeleteAll';
                    $methode = 'deleteRecipes';
                    break;
                case 'deleteAllCooks':
                    $namespace = 'controller\site\CookDeleteAll';
                    $methode = 'deleteCooks';
                    break;
                case 'deleteCook':
                    $namespace = 'controller\site\CookDelete';
                    $methode = 'deleteCook';
                    break;
                case 'deleteRecipe':
                    $namespace = 'controller\site\RecipeDelete';
                    $methode = 'deleteRecipe';
                    break;
                case 'modifyCook':
                    $namespace = 'controller\site\CookModify';
                    $methode = 'modifyCook';
                    break;
                case 'modifyRecipe':
                    $namespace = 'controller\site\RecipeModify';
                    $methode = 'modifyRecipe';
                    break;
                case 'addRecipe':
                    $namespace = 'controller\site\RecipeAdd';
                    $methode = 'addRecipe';
                    break;
                case 'addCook':
                    $namespace = 'controller\site\CookAdd';
                    $methode = 'addCook';
                    break;
                case 'auth':
                    $namespace = 'controller\admin\Auth';
                    $methode = 'authentify';
                    break;
                case 'user':
                    $namespace = 'controller\admin\Auth';
                    $methode = 'goToDashboard';
                    break;
                case 'deconnection':
                    unset($_SESSION);
                    session_destroy();
                    $namespace = 'controller\site\Home';
                    $methode = 'displayHome';
                    break;
            }

            # instantiate the Rooter class
            $rooter = new Rooter($namespace, $methode);
        } else {
            $rooter = new Rooter('controller\site\Home', 'displayHome');
        }
        $rooter->ChooseController();
    }


}