<?php
namespace classes;

class Rooter {
    private $namespace;
    private $methode;

    public function __construct($namespace, $methode){
        $this->namespace = $namespace;
        $this->methode = $methode;
    }

    public function ChooseController(){
        $namespace = $this->namespace;
        $methode = $this->methode;
        # ob_start();
        if(method_exists($namespace, $methode)) {
            #$breadcrumb = 'accueil';
            $objet = new $namespace();
            $objet->$methode();
        } else {
            echo 'Erreur chooseController';
        }
        #$content = ob_get_clean();

        #require VIEW.'/templates/default.php';
    }
}