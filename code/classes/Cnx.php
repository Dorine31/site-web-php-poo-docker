<?php

namespace classes;
use model\Manager;

class Cnx {
    private static $cnx;

    public static function getCnx(){
        if(self::$cnx === null){
            self::$cnx = new Manager(DBNAME, HOST, CNX_LOGIN, CNX_PASSWORD);
        }
        return self::$cnx;
    }

    public static function notFound(){
        header("HTTP/1.0 404 not Found");
        header('Location:index.php?p=404');
    }
}